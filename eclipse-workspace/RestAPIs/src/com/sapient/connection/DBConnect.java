package com.sapient.connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnect {

public static Connection co=null;
	
	public static Connection getConnection()throws Exception{
		if(co==null){
			Class.forName("oracle.jdbc.driver.OracleDriver");//registering the driver
			co=DriverManager.getConnection
					("jdbc:oracle:thin:@localhost:1521:XE","system","password");
			co.setAutoCommit(true);
		}
		return co;
	}
	
}