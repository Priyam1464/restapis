package com.sapient.rest.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sapient.connection.DBConnect;
import com.sapient.model.StudentBean;

/**
 * Servlet implementation class Student
 */
@Path("/student")
public class Student {	
	@GET
	@Path("/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getStudent(@PathParam("param")int id)
	{
		Connection co;
		StudentBean student=null;
		try {
			
			co = DBConnect.getConnection();
			String query="SELECT * FROM STUDENTS WHERE ID=?";
			PreparedStatement ps=co.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery(query);
			if(rs.next())
			{
				 student=new StudentBean(rs.getInt("id"),rs.getString("name"),rs.getInt("rNo"),rs.getString("percentage"));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity(student).build();
	
	}
	@PUT
    @Path("/update")
	@Produces(MediaType.TEXT_PLAIN)
	
	public Response editStudent( 
			@FormParam("id") int id,  
			 @FormParam("name") String name,
		      @FormParam("rollNo") int rNo,
		      @FormParam("percentage")String percentage
		    )
	{
		String output="";
		Connection co;
         try {
			
			co = DBConnect.getConnection();
			String query="UPDATE STUDENTS SET id=?,name=?,rNo=?,percentage=? WHERE rNo=?";
			PreparedStatement ps=co.prepareStatement(query);
			ps.setInt(1, id);
			ps.setString(2,name);
			ps.setInt(3,rNo);
			ps.setString(4,percentage);
			ResultSet rs=ps.executeQuery(query);
			if(rs.next())
			{
				output="Updated successfully";
			}else
			{
				output="Id Not found";
			}
             }
         catch(Exception e)
         {
        	 e.printStackTrace();
         }
         return Response.status(200).entity(output).build();
	}
	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteStudent(@PathParam("id") int id)
	{
		Connection co;
		String output="";
		try
		{
			co = DBConnect.getConnection();
			String query="DELETE FROM STUDENTS WHERE id=?";
			PreparedStatement ps=co.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery(query);
			if(rs.next())
			{
				output="Deleted successfully";
			}else
			{
				output="Id Not found";
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		 return Response.status(200).entity(output).build();
	}

}
