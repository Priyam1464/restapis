<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<%
com.player.model.StudentBean student=(com.player.model.StudentBean)request.getAttribute("student");
String id=String.valueOf(student.getId());
String rollNo=String.valueOf(student.getRollNo());
String name=student.getName();
String percentage=student.getPercentage();


%>
<body>
<center>
<div>
<form action="StudentInfo" method="post">
<table>
<tr>
<th>ID</th>
<td><input type="text" value="<%=id%>" name="id"/></td>
</tr>
<tr>
<th>Roll No</th>
<td><input type="text" value="<%=rollNo%>" name="rollNo"/></td>
</tr>
<tr>
<th>Name</th>
<td><input type="text" value="<%=name%>" name="name"/></td>
</tr>
<tr>
<th>Percentage</th>
<td><input type="text" value="<%=percentage%>" name="percentage"/></td>
</tr>
<tr>
<td><input type="radio" value="Edit" name="radio"/></th>
<td><input type="radio" value="Delete" name="radio"/></td>
</tr>
</table>
<input type="button" name="submit" value="submit"/>
</form>
</div>
</center>
</body>
</html>