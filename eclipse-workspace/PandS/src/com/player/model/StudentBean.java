package com.player.model;

public class StudentBean {
private int id,rNo;
private String name,rollNo,percentage;
public StudentBean(int id, String name, int rNo, String percentage) {
	super();
	this.id = id;
	this.name = name;
	this.rNo = rNo;
	this.percentage = percentage;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getRollNo() {
	return rollNo;
}
public void setRollNo(String rollNo) {
	this.rollNo = rollNo;
}
public String getPercentage() {
	return percentage;
}
public void setPercentage(String percentage) {
	this.percentage = percentage;
}

}
