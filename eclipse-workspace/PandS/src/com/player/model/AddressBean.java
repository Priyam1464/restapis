package com.player.model;

public class AddressBean {
private Double lat,lng;
public AddressBean(Double lat,Double lng)
{
	this.lat=lat;
	this.lng=lng;
}
public Double getLat() {
	return lat;
}
public Double getLng() {
	return lng;
}
}
