package com.player.model;

public class PlayerBean {
 private String name,imgUrl,country,age,profile;
 public PlayerBean(String name,String age,String country,String imgUrl,String profile)
 {
	 this.name=name;
	 this.age=age;
	 this.country=country;
	 this.imgUrl=imgUrl;
	 this.profile=profile;
 }
 public String getAge() {
	return age;
}
 public String getCountry() {
	return country;
}
 public String getImgUrl() {
	return imgUrl;
}
 public String getName() {
	return name;
}
 public String getProfile() {
	return profile;
}
}
