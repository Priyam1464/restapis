package com.sapient.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.player.model.StudentBean;

/**
 * Servlet implementation class StudentInfo
 */
public class StudentInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentInfo() {
        super();
        // TODO Auto-generated constructor stub
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	String radio=request.getParameter("radio");
	String name=request.getParameter("name");
	String rollNo=request.getParameter("rollNo");
	String percentage=request.getParameter("percentage");
	String id=request.getParameter("id");
	if(radio.equals("Edit"))
	{

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri());
		String msg=target.path("/update").request().accept(MediaType.TEXT_PLAIN).get(String.class);
		
		
	}else
	{
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri());
		String msg=target.path("/delete/"+Integer.parseInt(rollNo)+"/").request().accept(MediaType.TEXT_PLAIN).get(String.class);
	}
	}
	public static URI getBaseUri()
	{
		 return UriBuilder.fromUri("https://192.168.137.1/rest/student").build();
	}

}
