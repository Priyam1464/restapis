package com.sapient.web;

import java.io.IOException;
import java.net.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.player.model.StudentBean;

/**
 * Servlet implementation class Student
 */
public class Student extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String rNo=request.getParameter("sid");
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri());
		StudentBean student=target.path("/"+Integer.parseInt(rNo)).request().accept(MediaType.TEXT_PLAIN).get(StudentBean.class);
	     request.setAttribute("student",student);
	     RequestDispatcher requestDispatcher=request.getRequestDispatcher("StudentInfo.jsp");
	     requestDispatcher.forward(request,response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public static URI getBaseUri()
	{
		 return UriBuilder.fromUri("https://192.168.137.1/rest/student").build();
	}
}
