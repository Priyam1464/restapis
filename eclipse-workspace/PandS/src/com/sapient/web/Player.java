package com.sapient.web;

import java.io.IOException;
import java.math.BigDecimal;

import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

import com.player.model.PlayerBean;
/**
 * Servlet implementation class Player
 */
public class Player extends HttpServlet {
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Player() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	  String playerName=request.getParameter("player");
	  System.out.println(playerName);
	  ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri(playerName));
		JSONObject playerIdJson=target.request().accept(MediaType.APPLICATION_JSON).get(JSONObject.class);
	  
	    ArrayList<HashMap<String,Object>> jsonArray=(ArrayList<HashMap<String,Object>>) playerIdJson.get("data");
		System.out.println(jsonArray);
	    BigDecimal playerId=(BigDecimal)jsonArray.get(0).get("pid");
	    WebTarget playerTarget=client.target(getPlayerUri(String.valueOf(playerId)));
	    JSONObject playerData=playerTarget.request().accept(MediaType.APPLICATION_JSON).get(JSONObject.class);
	    String profile=(String) playerData.get("profile");
	    String fullName=(String)playerData.get("fullName");
	    String age=(String)playerData.get("currentAge");
	    String country=(String)playerData.get("country");
	    String imgUrl=(String)playerData.get("imageURL");
	    PlayerBean playerBean=new PlayerBean(fullName,age,country,imgUrl,profile);
	     request.setAttribute("player",playerBean);
	     RequestDispatcher requestDispatcher=request.getRequestDispatcher("PlayerInfo.jsp");
	     requestDispatcher.forward(request, response);
	}
	
	private static URI getBaseUri(String playerName) {
		String finalString="";
		 for(int i=0;i<playerName.length();i++)
		 {
			 if(playerName.charAt(i)==' ')finalString+="%20";
			 else finalString+=playerName.charAt(i);
		 }
		System.out.println(finalString);
		return UriBuilder.fromUri("https://cricapi.com/api/playerFinder?apikey=hDgHHdrVumaZa4eFj34fycF9mSu1&name="+finalString).build();
	}
	 private static URI getPlayerUri(String playerId)
	 {
		 
		 return UriBuilder.fromUri("https://cricapi.com/api/playerStats?apikey=hDgHHdrVumaZa4eFj34fycF9mSu1&pid="+playerId).build();
	 }
	
}
